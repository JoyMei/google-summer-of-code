---
title: Welcome to our 2022 GSoC Contributors! 
slug: welcome-contributors
author: Alexandra Marin, Christian Frisson
date: 2022-06-02 11:00:11 UTC-05:00
tags: contributions
type: text
---

We are pleased to announce that 3 contributors were selected for SAT's first year participating in GSoC 2022. 

We are excited to welcome in our team starting June 13:

* [Matthew Wiese](/authors/matthew-wiese/)
* [Vanshita Verma](/authors/vanshita-verma/)
* [Yash Raj](/authors/yash-raj/)