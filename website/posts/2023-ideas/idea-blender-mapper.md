<!--
.. title: Blender add-on to create maps between UI signals and external controllers with libmapper
.. slug: idea-blender-mapper
.. author: Christian Frisson
.. date: 2023-02-08 13:58:11 UTC-05:00
.. tags: ideas, easy, 175 hours, Blender, libmapper
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Blender add-on to create maps between UI signals and external controllers with libmapper

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

We use [Blender](https://www.blender.org/) extensively at the SAT, notably for: 
creating assets for projection mapping with [Splash](https://gitlab.com/sat-mtl/tools/splash/splash), 
animating avatars using pose estimation with [LivePose](https://gitlab.com/sat-mtl/tools/livepose) (with [livepose-blender-addon](https://gitlab.com/sat-mtl/metalab/tools/livepose-blender-addon) or [our fork of BlendArMocap](https://gitlab.com/sat-mtl/tools/forks/BlendArMocap)), 
editing demo videos with the Blender [Video Sequencer](https://docs.blender.org/manual/en/latest/editors/video_sequencer/introduction.html) (VSE) (check [SAT Metalab's Vimeo channel](https://vimeo.com/satmetalab)).
Blender supports 4 categories of [input devices](https://docs.blender.org/manual/en/latest/getting_started/configuration/hardware.html#input-devices) as controllers: keyboards, mice, graphic tablets, and NDOF devices (aka 3D Mouse, such as the [3Dconnexion SpaceNavigator](https://en.wikipedia.org/wiki/3Dconnexion)). 
The [Input Preferences](https://docs.blender.org/manual/en/latest/editors/preferences/input.html) panel lets Blender users configure these input devices through [keymaps](https://docs.blender.org/manual/en/latest/editors/preferences/keymap.html) and UI widgets whose properties and bindings are not extendable anymore, once Blender is compiled or at runtime. 
Community Blender add-ons such as [maybites blender.NodeOSC](https://github.com/maybites/blender.NodeOSC) and [JPfeP AddRoutes](https://github.com/JPfeP/AddRoutes) allow Blender users to control a subset of the Blender UI from external controllers compatible with inter-operability protocols [MIDI](https://en.wikipedia.org/wiki/MIDI) or [OSC](https://en.wikipedia.org/wiki/OSC). 

We also use [libmapper](https://github.com/libmapper/) as inter-operability protocol at the SAT to overcome the limitations in terms of mapping and networking of MIDI and OSC, to author immersive experiences and to map networks of digital musical instruments such as: 
[The T-Stick](https://github.com/idmil/t-stick) (a wireless graspable instrument with touch and force and orientation sensing), 
[TorqueTuner](https://github.com/idmil/torquetuner) (a force-feedback haptic device with 1 degree of freedom), 
[Probatio](https://probat.io/) (a modular physical and functional prototyping toolkit). 
[webmapper](https://github.com/libmapper/webmapper/) is a web-based UI for creating mappings with libmapper. 

The goal of this project idea is to develop a Blender addon (or fork an existing add-on such as [maybites blender.NodeOSC](https://github.com/maybites/blender.NodeOSC) and [JPfeP AddRoutes](https://github.com/JPfeP/AddRoutes)) to enable Blender users to create maps between Blender UI signals and external controllers with [libmapper](https://github.com/libmapper/).

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Create a Blender addon (or fork an existing add-on such as [maybites blender.NodeOSC](https://github.com/maybites/blender.NodeOSC) and [JPfeP AddRoutes](https://github.com/JPfeP/AddRoutes)) in Python
* Expose Blender UI signals with [libmapper](https://github.com/libmapper/) python bindings
* Create a video of a demo of mappings with the add-on and [webmapper](https://github.com/libmapper/webmapper/), between subsets of the Blender UI and external controllers 
* (Bonus) Explore mappings for browsing audio and video thumbnails in the Blender [Video Sequencer](https://docs.blender.org/manual/en/latest/editors/video_sequencer/introduction.html) (VSE), with mappings between features extracted from audio and video content and haptic feedback

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: experience with Python
* preferred: experience with Blender

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Christian Frisson](https://gitlab.com/christianfrisson), [Edu Meneses](https://gitlab.com/edumeneses)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
