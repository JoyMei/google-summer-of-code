<!--
.. title: Audio extension for the 3D scene exchange format glTF
.. slug: idea-audio-gltf
.. author: Nicolas Bouillot
.. date: 2023-02-07 10:49:11 UTC-05:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Definition and implementation of an audio extension for the 3D scene exchange format glTF

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

The [glTF](https://github.com/KhronosGroup/glTF) format is a standardized standard allowing the exchange of complex 3D scenes including 3D models, animations, cameras, etc. However, this format does not support the audio part. It is therefore necessary to define a dedicated extension to describe the sound aspects of a 3D scene.


## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Make a state of the art of audio scene description formats
* Participate in the definition of the specifications of an audio extension in glTF format
* Develop an implementation example
* Add support for the extension in the tools developed/used by the Metalab: [vaRays](https://gitlab.com/sat-mtl/tools/vaRays), [Blender](https://gitlab.com/sat-mtl/tools/forks/blender)
* Document the work and ensure its maintainability.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: experience with Linux, Python, C++, Free and Open Source Software (FOSS)
* preferred: experience with [Blender](https://blender.org)

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[Michał Seta](https://gitlab.com/djiamnot), more?

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium
