# Google Summer of Code at SAT

This repository contains project ideas and proposals for the Google Summer of Code at the [Society for Arts and Technology](https://sat.qc.ca/).

The static website is generated with [nikola](https://getnikola.com/) and deployed to gitlab pages using [a gitlab CI configuration](.gitlab-ci.yml)

## Installation
### Recommended installation process (python3 venv)

Use python3 venv in the `nikola-venv` directory at the root of this repo

```bash
sudo apt install python3-venv
python3 -m venv nikola-venv
cd nikola-venv/
source bin/activate
bin/python -m pip install -U pip setuptools wheel
bin/python -m pip install -U "Nikola[extras]"
cd ..
```
Done !

### Using venv

Before running the nikola command, activate the python3 venv :
```bash
source nikola-venv/bin/activate
```
## Browse the website locally before pushing to gitlab

From the website folder, run `nikola auto --browser` to build and run the server automatically, detect site changes and rebuild, and refresh the browser:
    
```
cd website
nikola auto --browser
```

This should open the website in your default browser. If not, use this url : `http://127.0.0.1:8000/`

## More information about nikola

[Getting started](https://getnikola.com/getting-started.html) (10 min. read)

## Contributing
### Project ideas


#### Target audience

Project ideas are mainly written by Mentors and Org Admins. 
Contributors and new/external Mentors are welcome to suggest project ideas in collaboration with existing Mentors and Org Admins.

We encourage participation from Contributors and Mentors from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).

#### Instructions

##### First step: open a gitlab issue

First [open an issue](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues) to discuss with us.
Existing Mentors and Org Admins will invite Contributors and new/external Mentors to join this repository with Developer role, so that they can push to branches and use our continuous integration (CI) nodes to validate your merge request.

##### Second step: open a merge request

In your issue, click on the blue button `Create merge request` to open a merge request linked to your issue.

Two options for the remaining instructions: 
1. you can clone this repository if you are familiar with the process of cloning and branching and requesting merge requests, 
2. or (recommended) you can navigate to the branch linked on top of the page of your merge request and then click on [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) button.

Copy [idea-template.md](idea-template.md) into the [2023-ideas](website/posts/2023-ideas) folder in your merge request branch: `website/posts/2023-ideas/idea-title.md`

Then rename `idea-title.md` by replacing `title` by a dash-separated list of lowercase words.

Write your project idea, commit and push your contributions.

Once your project idea is ready for review, mark your merge request as ready, so that we can discuss your project idea.

### Proposals

#### Target audience

Proposal are submitted by Contributors and reviewed by Mentors and Org Admins. 

We encourage participation from Contributors from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).

#### Instructions

##### First step: check if you are eligible

Please first:

* confirm that you are eligible to participate as student contributor by reading section `7. GSoC Contributors` of GSoC rules: [https://summerofcode.withgoogle.com/rules](https://summerofcode.withgoogle.com/rules)
* confirm that you are available during the GSoC timeline: [https://developers.google.com/open-source/gsoc/timeline](https://developers.google.com/open-source/gsoc/timeline)
* read the GSOC contributor guide: [https://google.github.io/gsocguides/student/](https://google.github.io/gsocguides/student/)
* browse our project ideas at SAT: [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)
* read our proposals guide at SAT: [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-welcome/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-welcome/)

##### Second step: open a gitlab issue

Then please open an issue to reach out to us:

* sign [up](https://gitlab.com/users/sign_up)/[in](https://gitlab.com/users/sign_in) on [gitlab.com](https://gitlab.com)
* create an issue on: [https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues)

We will then work together with you Contributors so that you can submit your proposal, first as a [merge request to our repository](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/merge_requests) and then by applying on [https://summerofcode.withgoogle.com](https://summerofcode.withgoogle.com) as required by the GSoC program. 
We will invite you to join this repository with Developer role, so that you can push to branches and use our continuous integration (CI) nodes to validate your merge request.

##### Third step: open a merge request

In your issue, click on the blue button `Create merge request` to open a merge request linked to your issue.

Two options for remaining instructions: 
1. you can clone this repository if you are familiar with the process of cloning and branching and requesting merge requests, 
2. or (recommended) you can navigate to the branch linked on top of the page of your merge request and then click on [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) button.

Copy [proposal-template.md](proposal-template.md) into the [2023-proposals](website/posts/2023-proposals) folder in your merge request branch: `website/posts/2023-proposals/proposal-username-title.md`

Then rename `proposal-username-title.md` by replacing `username` by your gitlab username and `title` by your proposal title, the whole filename should be a dash-separated list of lowercase words. 

Edit your proposal file and follow the instructions formatted as inline HTML comments.

While you write your proposal, regularly commit and push your contributions.

Once your proposal is ready for review, mark your merge request as ready, so that we can discuss your proposal idea. 

Google requires proposals to be submitted through their online forms, including a PDF file. Please check our [proposal forms walkthrough](website/posts/2023-proposals/proposal-forms.md). We automate the production of a PDF file for each proposal in markdown format through our [gitlab CI config](.gitlab-ci.yml). These PDF files are available as artifacts from our [gitlab CI pipelines](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/pipelines). To test the production of PDF files locally, with Ubuntu 22.04 LTS, please first run `./install.sh` then `./build-proposal.sh website/posts/2023-proposals/proposal-welcome.md` with the filename adapted to your proposal. 

<!-- 

### Communication

From: https://developers.google.com/open-source/gsoc/resources/marketing#template_email_for_gsoc_applicants

> Do you know anyone who might be a good candidate to participate as a contributor in GSoC? You can use the sample email below to help advertise the program. Consider sending it to appropriate schools, teachers, friends, clubs, developer communities, etc.

Please use this communication template adapted for SAT: [website/posts/2022-share.md](website/posts/2022-share.md)

### Contributions

Every contributor has their own folder under `website/posts/2022-contributions/` where they can publish:
* blog posts: please copy [blog-post-template.md](blog-post-template.md) into your folder under `website/posts/2022-contributions/` with `YYYY-MM-dd-username-title` for filename and `slug` (replacing `YYYY-MM-dd` by the date, `username` by your gitlab username and `title` by your blog post title, the whole filename should be a dash-separated list of lowercase words) and customize metadata and contents.
* [work products](https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission): please copy [work-product-template.md](work-product-template.md) into your folder under `website/posts/2022-contributions/` with as filename `username-work-product.md` (replacing `username` by your gitlab username) and customize metadata and contents. 

-->
